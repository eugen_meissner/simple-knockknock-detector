// Initialize app
var myApp = new Framework7();

var FRAME_SIZE = 1500;
var G_EARTH = 9.80665;
var C_DT = 0.18;
var FREQ = 33;
var D_LENGTH = Math.ceil(FRAME_SIZE / FREQ);
var THRES_MIN_ACT = G_EARTH / 4.0;
var THRES_KNOCK = G_EARTH / 2.0;
var W_SIZE = Math.ceil(FRAME_SIZE * 0.5 / FREQ);

var myGraph = new Graph({
    canvasId: 'myCanvas',
    minX: 0,
    minY: -10,
    maxX: D_LENGTH,
    maxY: 10,
    unitsPerTick: 1
});

var dt_0 = new Date();
var data = [];
var peakCounter = 0;
var knockCounter = 0;
var output = [0.0,0.0,0.0];
var counter = 1;
var avgAmpl = 0.0;

// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});

// Handle Cordova Device Ready Event
$$(document).on('deviceready', function() {
    console.log("Device is ready!");
    startWatch();
});


// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
myApp.onPageInit('about', function (page) {
    // Do something here for "about" page

})

// Option 2. Using one 'pageInit' event handler for all pages:
$$(document).on('pageInit', function (e) {
    // Get page data from event data
    var page = e.detail.page;

    if (page.name === 'about') {
        // Following code will be executed for page with data-page attribute equal to "about"
        myApp.alert('Here comes About page');
    }
})

// Option 2. Using live 'pageInit' event handlers for each page
$$(document).on('pageInit', '.page[data-page="about"]', function (e) {
    // Following code will be executed for page with data-page attribute equal to "about"
    myApp.alert('Here comes About page');
})

function startWatch() {
    var options = { frequency: FREQ };
    watchID = navigator.accelerometer.watchAcceleration(onSuccess, onError, options);
}

function stopWatch() {
    if (watchID) {
        navigator.accelerometer.clearWatch(watchID);
        watchID = null;
    }
}

function onSuccess(acceleration) {
    var element = document.getElementById('accelerometer');

    var dt = 1.0 / (counter / ((acceleration.timestamp - dt_0) / Math.pow(10.0,10)));

    var alpha = C_DT / (C_DT + dt);

    output[0] = alpha * output[0] + (1.0 - alpha) * acceleration.x;
    output[1] = alpha * output[1] + (1.0 - alpha) * acceleration.y;
    output[2] = alpha * output[2] + (1.0 - alpha) * acceleration.z;

    var a = Math.sqrt(
        Math.pow(acceleration.x,2.0) + 
        Math.pow(acceleration.y,2.0) + 
        Math.pow(acceleration.z,2.0)
    ) - G_EARTH;

    var f = Math.sqrt(
        Math.pow(output[0],2.0) + 
        Math.pow(output[1],2.0) + 
        Math.pow(output[2],2.0)
    );

    var la = Math.abs(a - f);

    var peaks = new Array();
    var knocks = new Array();
    var act_c = 0;

    if(data.length == D_LENGTH) data.shift();
    data.push(la);

    if(counter % W_SIZE == 0){
        // detect peaks
        for(var i=1; i<W_SIZE-1; i++){
            // there is is a min value for activities
            // emperical value ;) to rec. some act. on device
            if(data[i] > THRES_MIN_ACT && data[i] < THRES_KNOCK){
                act_c++;
                continue;
            }
            // emperical value ;) this is the thres for a knock :P
            if(data[i] < THRES_KNOCK) continue;
            if(data[i-1] < data[i] && data[i] > data[i+1]){
                avgAmpl += data[i];
                avgAmpl *= 0.5;
                peaks.push(i);
                peakCounter++;
            }
        }

        // emperical value ;) if more then 10% activity happens on device ignore
        if(act_c <= Math.ceil(D_LENGTH*0.1) && peaks.length == 2){
            for(var i=1; i<peaks.length; i++){
                var dtp = (peaks[i] - peaks[i-1]) * FREQ;
                // emperical value ;)
                if(dtp >= 50 && dtp <= 750){
                    knocks.push(true);
                    knockCounter++;
                }
            }
        }
    }

    dt_0 = acceleration.timestamp;
    counter++;
    
    element.innerHTML = 
    'L-Accel:' + la + '<br />' +
    'Avg-Ampl:' + avgAmpl + '<br />' +
    'Peaks: ' + peakCounter + '<br />' +
    'Knock-Knock-Counter: ' + knockCounter;
    myGraph.draw(data, 'green', 1);
}

function onError() {
    alert('onError!');
}